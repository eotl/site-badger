FROM alpine:3.19.0

ARG BIN_TARGET=/usr/local/bin
ARG HUGO_VERSION=0.143.1
ARG UID=1000
ARG WEBHOOKD_VERSION=1.20.1
ENV HUGO_GIT_URL=
ENV HUGO_PUBLIC_DIR=/root/public
ENV HUGO_WORKING_DIR=/root/website

RUN apk add --no-cache \
  bash \
  curl \
  git \
  go \
  libc6-compat \
  openssh-client \
  tzdata

RUN curl -o webhookd.tgz --fail -L "https://github.com/ncarlier/webhookd/releases/download/v${WEBHOOKD_VERSION}/webhookd-linux-amd64.tgz" && \
  tar xvzf webhookd.tgz -C $BIN_TARGET

RUN curl -o hugo.tgz --fail -L "https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_extended_${HUGO_VERSION}_linux-amd64.tar.gz" && \
  tar xvzf hugo.tgz -C $BIN_TARGET

RUN rm webhookd.tgz && \
  rm hugo.tgz && \
  rm $BIN_TARGET/README.md && \
  rm $BIN_TARGET/CHANGELOG.md && \
  rm $BIN_TARGET/LICENSE

EXPOSE 8080

RUN mkdir $HUGO_PUBLIC_DIR

CMD ["webhookd"]

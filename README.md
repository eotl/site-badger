Site Badger
===========

A Docker Image for generating static websites with [Hugo](https://gohugo.io)
and a listener service which triggers git pulls and builds via
[webhookd](https://github.com/ncarlier/webhookd).


## Use

This Docker Package was created and published so that it can be used in a [Co-op Cloud Recipe](https://docs.coopcloud.tech). The recipe that deploys this package is: [parasol-static-site](https://git.coopcloud.tech/coop-cloud/parasol-static-site).


## Hack

Make desired changes to `Dockerfile` and test them locally. 
Then Git commit when you are satisfied with your work.

To update the published package, do the following commands on your machine:

```
docker build -t codeberg.org/eotl/site-badger:0.2.0 .
docker push codeberg.org/eotl/site-badger:0.1.0
```


## License

The following starter was made by [@adz](https://adz.garden) for [offline.place](https://offline.place) with the following license:

```
UNIVERSAL PUBLIC DOMAIN LICENSE

This software and everything else in the universe is in the public domain. Ideas are not property.
```
